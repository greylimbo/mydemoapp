import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { NiftyappEmployeeNiftyModule } from './employee/employee-nifty.module';
import { NiftyappPayslipNiftyModule } from './payslip/payslip-nifty.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        NiftyappEmployeeNiftyModule,
        NiftyappPayslipNiftyModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NiftyappEntityModule {}
