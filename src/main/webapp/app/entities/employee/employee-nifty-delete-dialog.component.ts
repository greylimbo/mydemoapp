import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { EmployeeNifty } from './employee-nifty.model';
import { EmployeeNiftyPopupService } from './employee-nifty-popup.service';
import { EmployeeNiftyService } from './employee-nifty.service';

@Component({
    selector: 'jhi-employee-nifty-delete-dialog',
    templateUrl: './employee-nifty-delete-dialog.component.html'
})
export class EmployeeNiftyDeleteDialogComponent {

    employee: EmployeeNifty;

    constructor(
        private employeeService: EmployeeNiftyService,
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.employeeService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'employeeListModification',
                content: 'Deleted an employee'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('niftyappApp.employee.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-employee-nifty-delete-popup',
    template: ''
})
export class EmployeeNiftyDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private employeePopupService: EmployeeNiftyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.employeePopupService
                .open(EmployeeNiftyDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
