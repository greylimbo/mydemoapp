import { BaseEntity } from './../../shared';

const enum Language {
    'ENGLISH',
    'AFRIKAANS',
    'ZULU',
    'XHOSA',
    'SWATI',
    'OTHER'
}

export class EmployeeNifty implements BaseEntity {
    constructor(
        public id?: number,
        public firstName?: string,
        public lastName?: string,
        public employeeNumber?: string,
        public email?: string,
        public phoneNumber?: string,
        public startDate?: any,
        public terminationDate?: any,
        public salary?: number,
        public pensionContribution?: number,
        public primaryLanguage?: Language,
        public secondaryLanguage?: Language,
        public payslips?: BaseEntity[],
    ) {
    }
}
