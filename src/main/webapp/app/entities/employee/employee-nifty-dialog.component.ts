import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EmployeeNifty } from './employee-nifty.model';
import { EmployeeNiftyPopupService } from './employee-nifty-popup.service';
import { EmployeeNiftyService } from './employee-nifty.service';

@Component({
    selector: 'jhi-employee-nifty-dialog',
    templateUrl: './employee-nifty-dialog.component.html'
})
export class EmployeeNiftyDialogComponent implements OnInit {

    employee: EmployeeNifty;
    authorities: any[];
    isSaving: boolean;
    startDateDp: any;
    terminationDateDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private employeeService: EmployeeNiftyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.employee.id !== undefined) {
            this.subscribeToSaveResponse(
                this.employeeService.update(this.employee), false);
        } else {
            this.subscribeToSaveResponse(
                this.employeeService.create(this.employee), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<EmployeeNifty>, isCreated: boolean) {
        result.subscribe((res: EmployeeNifty) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: EmployeeNifty, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'niftyappApp.employee.created'
            : 'niftyappApp.employee.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'employeeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }
}

@Component({
    selector: 'jhi-employee-nifty-popup',
    template: ''
})
export class EmployeeNiftyPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private employeePopupService: EmployeeNiftyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.employeePopupService
                    .open(EmployeeNiftyDialogComponent, params['id']);
            } else {
                this.modalRef = this.employeePopupService
                    .open(EmployeeNiftyDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
