import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { EmployeeNiftyComponent } from './employee-nifty.component';
import { EmployeeNiftyDetailComponent } from './employee-nifty-detail.component';
import { EmployeeNiftyPopupComponent } from './employee-nifty-dialog.component';
import { EmployeeNiftyDeletePopupComponent } from './employee-nifty-delete-dialog.component';

import { Principal } from '../../shared';

export const employeeRoute: Routes = [
    {
        path: 'employee-nifty',
        component: EmployeeNiftyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.employee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'employee-nifty/:id',
        component: EmployeeNiftyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.employee.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const employeePopupRoute: Routes = [
    {
        path: 'employee-nifty-new',
        component: EmployeeNiftyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.employee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'employee-nifty/:id/edit',
        component: EmployeeNiftyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.employee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'employee-nifty/:id/delete',
        component: EmployeeNiftyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.employee.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
