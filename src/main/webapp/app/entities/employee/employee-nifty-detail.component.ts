import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { EmployeeNifty } from './employee-nifty.model';
import { EmployeeNiftyService } from './employee-nifty.service';

import { PayslipNifty } from '../payslip/payslip-nifty.model';

@Component({
    selector: 'jhi-employee-nifty-detail',
    templateUrl: './employee-nifty-detail.component.html'
})
export class EmployeeNiftyDetailComponent implements OnInit, OnDestroy {

    employee: EmployeeNifty;
    payslips: PayslipNifty;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private employeeService: EmployeeNiftyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEmployees();
    }

    load(id) {
        this.employeeService.find(id).subscribe((employee) => {
            this.employee = employee;
            this.payslips = this.employee.payslips;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEmployees() {
        this.eventSubscriber = this.eventManager.subscribe(
            'employeeListModification',
            (response) => this.load(this.employee.id)
        );
    }
}
