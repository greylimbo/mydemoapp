import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NiftyappSharedModule } from '../../shared';
import {
    EmployeeNiftyService,
    EmployeeNiftyPopupService,
    EmployeeNiftyComponent,
    EmployeeNiftyDetailComponent,
    EmployeeNiftyDialogComponent,
    EmployeeNiftyPopupComponent,
    EmployeeNiftyDeletePopupComponent,
    EmployeeNiftyDeleteDialogComponent,
    employeeRoute,
    employeePopupRoute,
} from './';

const ENTITY_STATES = [
    ...employeeRoute,
    ...employeePopupRoute,
];

@NgModule({
    imports: [
        NiftyappSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        EmployeeNiftyComponent,
        EmployeeNiftyDetailComponent,
        EmployeeNiftyDialogComponent,
        EmployeeNiftyDeleteDialogComponent,
        EmployeeNiftyPopupComponent,
        EmployeeNiftyDeletePopupComponent,
    ],
    entryComponents: [
        EmployeeNiftyComponent,
        EmployeeNiftyDialogComponent,
        EmployeeNiftyPopupComponent,
        EmployeeNiftyDeleteDialogComponent,
        EmployeeNiftyDeletePopupComponent,
    ],
    providers: [
        EmployeeNiftyService,
        EmployeeNiftyPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NiftyappEmployeeNiftyModule {}
