import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeNifty } from './employee-nifty.model';
import { EmployeeNiftyService } from './employee-nifty.service';

@Injectable()
export class EmployeeNiftyPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private employeeService: EmployeeNiftyService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.employeeService.find(id).subscribe((employee) => {
                if (employee.startDate) {
                    employee.startDate = {
                        year: employee.startDate.getFullYear(),
                        month: employee.startDate.getMonth() + 1,
                        day: employee.startDate.getDate()
                    };
                }
                if (employee.terminationDate) {
                    employee.terminationDate = {
                        year: employee.terminationDate.getFullYear(),
                        month: employee.terminationDate.getMonth() + 1,
                        day: employee.terminationDate.getDate()
                    };
                }
                this.employeeModalRef(component, employee);
            });
        } else {
            return this.employeeModalRef(component, new EmployeeNifty());
        }
    }

    employeeModalRef(component: Component, employee: EmployeeNifty): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.employee = employee;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
