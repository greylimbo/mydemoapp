import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { EmployeeNifty } from './employee-nifty.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class EmployeeNiftyService {

    private resourceUrl = 'api/employees';
    private resourceSearchUrl = 'api/_search/employees';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(employee: EmployeeNifty): Observable<EmployeeNifty> {
        const copy = this.convert(employee);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(employee: EmployeeNifty): Observable<EmployeeNifty> {
        const copy = this.convert(employee);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<EmployeeNifty> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.startDate = this.dateUtils
            .convertLocalDateFromServer(entity.startDate);
        entity.terminationDate = this.dateUtils
            .convertLocalDateFromServer(entity.terminationDate);
    }

    private convert(employee: EmployeeNifty): EmployeeNifty {
        const copy: EmployeeNifty = Object.assign({}, employee);
        copy.startDate = this.dateUtils
            .convertLocalDateToServer(employee.startDate);
        copy.terminationDate = this.dateUtils
            .convertLocalDateToServer(employee.terminationDate);
        return copy;
    }
}
