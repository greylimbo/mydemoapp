import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { Observable } from 'rxjs/Rx';
import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PayslipNifty } from './payslip-nifty.model';
import { PayslipNiftyPopupService } from './payslip-nifty-popup.service';
import { PayslipNiftyService } from './payslip-nifty.service';
import { EmployeeNifty, EmployeeNiftyService } from '../employee';
import { ResponseWrapper } from '../../shared';

@Component({
    selector: 'jhi-payslip-nifty-dialog',
    templateUrl: './payslip-nifty-dialog.component.html'
})
export class PayslipNiftyDialogComponent implements OnInit {

    payslip: PayslipNifty;
    authorities: any[];
    isSaving: boolean;

    employees: EmployeeNifty[];
    payPeriodDp: any;

    constructor(
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private payslipService: PayslipNiftyService,
        private employeeService: EmployeeNiftyService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.employeeService.query()
            .subscribe((res: ResponseWrapper) => { this.employees = res.json; }, (res: ResponseWrapper) => this.onError(res.json));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.payslip.id !== undefined) {
            this.subscribeToSaveResponse(
                this.payslipService.update(this.payslip), false);
        } else {
            this.subscribeToSaveResponse(
                this.payslipService.create(this.payslip), true);
        }
    }

    private subscribeToSaveResponse(result: Observable<PayslipNifty>, isCreated: boolean) {
        result.subscribe((res: PayslipNifty) =>
            this.onSaveSuccess(res, isCreated), (res: Response) => this.onSaveError(res));
    }

    private onSaveSuccess(result: PayslipNifty, isCreated: boolean) {
        this.alertService.success(
            isCreated ? 'niftyappApp.payslip.created'
            : 'niftyappApp.payslip.updated',
            { param : result.id }, null);

        this.eventManager.broadcast({ name: 'payslipListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        try {
            error.json();
        } catch (exception) {
            error.message = error.text();
        }
        this.isSaving = false;
        this.onError(error);
    }

    private onError(error) {
        this.alertService.error(error.message, null, null);
    }

    trackEmployeeById(index: number, item: EmployeeNifty) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-payslip-nifty-popup',
    template: ''
})
export class PayslipNiftyPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private payslipPopupService: PayslipNiftyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.modalRef = this.payslipPopupService
                    .open(PayslipNiftyDialogComponent, params['id']);
            } else {
                this.modalRef = this.payslipPopupService
                    .open(PayslipNiftyDialogComponent);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
