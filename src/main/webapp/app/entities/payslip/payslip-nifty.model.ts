import { BaseEntity } from './../../shared';

export class PayslipNifty implements BaseEntity {
    constructor(
        public id?: number,
        public payPeriod?: any,
        public grossIncome?: number,
        public incomeTax?: number,
        public netIncome?: number,
        public pension?: number,
        public employeeId?: number,
    ) {
    }
}
