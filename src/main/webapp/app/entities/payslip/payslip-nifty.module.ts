import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { NiftyappSharedModule } from '../../shared';
import {
    PayslipNiftyService,
    PayslipNiftyPopupService,
    PayslipNiftyComponent,
    PayslipNiftyDetailComponent,
    PayslipNiftyDialogComponent,
    PayslipNiftyPopupComponent,
    PayslipNiftyDeletePopupComponent,
    PayslipNiftyDeleteDialogComponent,
    payslipRoute,
    payslipPopupRoute,
} from './';

const ENTITY_STATES = [
    ...payslipRoute,
    ...payslipPopupRoute,
];

@NgModule({
    imports: [
        NiftyappSharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        PayslipNiftyComponent,
        PayslipNiftyDetailComponent,
        PayslipNiftyDialogComponent,
        PayslipNiftyDeleteDialogComponent,
        PayslipNiftyPopupComponent,
        PayslipNiftyDeletePopupComponent,
    ],
    entryComponents: [
        PayslipNiftyComponent,
        PayslipNiftyDialogComponent,
        PayslipNiftyPopupComponent,
        PayslipNiftyDeleteDialogComponent,
        PayslipNiftyDeletePopupComponent,
    ],
    providers: [
        PayslipNiftyService,
        PayslipNiftyPopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NiftyappPayslipNiftyModule {}
