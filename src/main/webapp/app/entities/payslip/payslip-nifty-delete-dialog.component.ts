import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';

import { PayslipNifty } from './payslip-nifty.model';
import { PayslipNiftyPopupService } from './payslip-nifty-popup.service';
import { PayslipNiftyService } from './payslip-nifty.service';

@Component({
    selector: 'jhi-payslip-nifty-delete-dialog',
    templateUrl: './payslip-nifty-delete-dialog.component.html'
})
export class PayslipNiftyDeleteDialogComponent {

    payslip: PayslipNifty;

    constructor(
        private payslipService: PayslipNiftyService,
        public activeModal: NgbActiveModal,
        private alertService: JhiAlertService,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.payslipService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'payslipListModification',
                content: 'Deleted an payslip'
            });
            this.activeModal.dismiss(true);
        });
        this.alertService.success('niftyappApp.payslip.deleted', { param : id }, null);
    }
}

@Component({
    selector: 'jhi-payslip-nifty-delete-popup',
    template: ''
})
export class PayslipNiftyDeletePopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private payslipPopupService: PayslipNiftyPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.modalRef = this.payslipPopupService
                .open(PayslipNiftyDeleteDialogComponent, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
