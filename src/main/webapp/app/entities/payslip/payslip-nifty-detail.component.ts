import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Rx';
import { JhiEventManager  } from 'ng-jhipster';

import { PayslipNifty } from './payslip-nifty.model';
import { PayslipNiftyService } from './payslip-nifty.service';

@Component({
    selector: 'jhi-payslip-nifty-detail',
    templateUrl: './payslip-nifty-detail.component.html'
})
export class PayslipNiftyDetailComponent implements OnInit, OnDestroy {

    payslip: PayslipNifty;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private payslipService: PayslipNiftyService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPayslips();
    }

    load(id) {
        this.payslipService.find(id).subscribe((payslip) => {
            this.payslip = payslip;
        });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPayslips() {
        this.eventSubscriber = this.eventManager.subscribe(
            'payslipListModification',
            (response) => this.load(this.payslip.id)
        );
    }
}
