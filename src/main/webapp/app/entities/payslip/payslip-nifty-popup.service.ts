import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { PayslipNifty } from './payslip-nifty.model';
import { PayslipNiftyService } from './payslip-nifty.service';

@Injectable()
export class PayslipNiftyPopupService {
    private isOpen = false;
    constructor(
        private modalService: NgbModal,
        private router: Router,
        private payslipService: PayslipNiftyService

    ) {}

    open(component: Component, id?: number | any): NgbModalRef {
        if (this.isOpen) {
            return;
        }
        this.isOpen = true;

        if (id) {
            this.payslipService.find(id).subscribe((payslip) => {
                if (payslip.payPeriod) {
                    payslip.payPeriod = {
                        year: payslip.payPeriod.getFullYear(),
                        month: payslip.payPeriod.getMonth() + 1,
                        day: payslip.payPeriod.getDate()
                    };
                }
                this.payslipModalRef(component, payslip);
            });
        } else {
            return this.payslipModalRef(component, new PayslipNifty());
        }
    }

    payslipModalRef(component: Component, payslip: PayslipNifty): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.payslip = payslip;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true });
            this.isOpen = false;
        });
        return modalRef;
    }
}
