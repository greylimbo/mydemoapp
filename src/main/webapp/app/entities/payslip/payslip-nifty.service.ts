import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils } from 'ng-jhipster';

import { PayslipNifty } from './payslip-nifty.model';
import { ResponseWrapper, createRequestOption } from '../../shared';

@Injectable()
export class PayslipNiftyService {

    private resourceUrl = 'api/payslips';
    private resourceSearchUrl = 'api/_search/payslips';

    constructor(private http: Http, private dateUtils: JhiDateUtils) { }

    create(payslip: PayslipNifty): Observable<PayslipNifty> {
        const copy = this.convert(payslip);
        return this.http.post(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    update(payslip: PayslipNifty): Observable<PayslipNifty> {
        const copy = this.convert(payslip);
        return this.http.put(this.resourceUrl, copy).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    find(id: number): Observable<PayslipNifty> {
        return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
            const jsonResponse = res.json();
            this.convertItemFromServer(jsonResponse);
            return jsonResponse;
        });
    }

    query(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceUrl, options)
            .map((res: Response) => this.convertResponse(res));
    }

    delete(id: number): Observable<Response> {
        return this.http.delete(`${this.resourceUrl}/${id}`);
    }

    search(req?: any): Observable<ResponseWrapper> {
        const options = createRequestOption(req);
        return this.http.get(this.resourceSearchUrl, options)
            .map((res: any) => this.convertResponse(res));
    }

    private convertResponse(res: Response): ResponseWrapper {
        const jsonResponse = res.json();
        for (let i = 0; i < jsonResponse.length; i++) {
            this.convertItemFromServer(jsonResponse[i]);
        }
        return new ResponseWrapper(res.headers, jsonResponse, res.status);
    }

    private convertItemFromServer(entity: any) {
        entity.payPeriod = this.dateUtils
            .convertLocalDateFromServer(entity.payPeriod);
    }

    private convert(payslip: PayslipNifty): PayslipNifty {
        const copy: PayslipNifty = Object.assign({}, payslip);
        copy.payPeriod = this.dateUtils
            .convertLocalDateToServer(payslip.payPeriod);
        return copy;
    }
}
