import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { JhiPaginationUtil } from 'ng-jhipster';

import { PayslipNiftyComponent } from './payslip-nifty.component';
import { PayslipNiftyDetailComponent } from './payslip-nifty-detail.component';
import { PayslipNiftyPopupComponent } from './payslip-nifty-dialog.component';
import { PayslipNiftyDeletePopupComponent } from './payslip-nifty-delete-dialog.component';

import { Principal } from '../../shared';

export const payslipRoute: Routes = [
    {
        path: 'payslip-nifty',
        component: PayslipNiftyComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.payslip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payslip-nifty/:id',
        component: PayslipNiftyDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.payslip.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const payslipPopupRoute: Routes = [
    {
        path: 'payslip-nifty-new',
        component: PayslipNiftyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.payslip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payslip-nifty/:id/edit',
        component: PayslipNiftyPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.payslip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payslip-nifty/:id/delete',
        component: PayslipNiftyDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'niftyappApp.payslip.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
