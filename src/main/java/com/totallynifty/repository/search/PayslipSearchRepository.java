package com.totallynifty.repository.search;

import com.totallynifty.domain.Payslip;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Payslip entity.
 */
public interface PayslipSearchRepository extends ElasticsearchRepository<Payslip, Long> {
}
