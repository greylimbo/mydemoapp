package com.totallynifty.repository;

import com.totallynifty.domain.Payslip;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Payslip entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PayslipRepository extends JpaRepository<Payslip,Long> {
    
}
