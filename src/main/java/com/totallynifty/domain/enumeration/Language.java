package com.totallynifty.domain.enumeration;

/**
 * The Language enumeration.
 */
public enum Language {
    ENGLISH, AFRIKAANS, ZULU, XHOSA, SWATI, OTHER
}
