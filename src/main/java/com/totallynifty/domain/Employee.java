package com.totallynifty.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

import com.totallynifty.domain.enumeration.Language;

/**
 * The Employee entity.
 */
@ApiModel(description = "The Employee entity.")
@Entity
@Table(name = "employee")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "employee")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * The firstname attribute.
     */
    @NotNull
    @ApiModelProperty(value = "The firstname attribute.", required = true)
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    private String lastName;

    @NotNull
    @Size(min = 5, max = 5)
    @Column(name = "employee_number", length = 5, nullable = false)
    private String employeeNumber;

    @NotNull
    @Size(min = 5, max = 150)
    @Column(name = "email", length = 150, nullable = false)
    private String email;

    @Size(min = 5, max = 12)
    @Column(name = "phone_number", length = 12)
    private String phoneNumber;

    @NotNull
    @Column(name = "start_date", nullable = false)
    private LocalDate startDate;

    @Column(name = "termination_date")
    private LocalDate terminationDate;

    @NotNull
    @DecimalMin(value = "0")
    @Column(name = "salary", precision=10, scale=2, nullable = false)
    private BigDecimal salary;

    @NotNull
    @Min(value = 0)
    @Max(value = 50)
    @Column(name = "pension_contribution", nullable = false)
    private Integer pensionContribution;

    @Enumerated(EnumType.STRING)
    @Column(name = "primary_language")
    private Language primaryLanguage;

    @Enumerated(EnumType.STRING)
    @Column(name = "secondary_language")
    private Language secondaryLanguage;

    @OneToMany(mappedBy = "employee",fetch = FetchType.EAGER)
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Payslip> payslips = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public Employee employeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
        return this;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmail() {
        return email;
    }

    public Employee email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public Employee phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Employee startDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public Employee terminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
        return this;
    }

    public void setTerminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public Employee salary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Integer getPensionContribution() {
        return pensionContribution;
    }

    public Employee pensionContribution(Integer pensionContribution) {
        this.pensionContribution = pensionContribution;
        return this;
    }

    public void setPensionContribution(Integer pensionContribution) {
        this.pensionContribution = pensionContribution;
    }

    public Language getPrimaryLanguage() {
        return primaryLanguage;
    }

    public Employee primaryLanguage(Language primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
        return this;
    }

    public void setPrimaryLanguage(Language primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public Language getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public Employee secondaryLanguage(Language secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
        return this;
    }

    public void setSecondaryLanguage(Language secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public Set<Payslip> getPayslips() {
        return payslips;
    }

    public Employee payslips(Set<Payslip> payslips) {
        this.payslips = payslips;
        return this;
    }

    public Employee addPayslip(Payslip payslip) {
        this.payslips.add(payslip);
        payslip.setEmployee(this);
        return this;
    }

    public Employee removePayslip(Payslip payslip) {
        this.payslips.remove(payslip);
        payslip.setEmployee(null);
        return this;
    }

    public void setPayslips(Set<Payslip> payslips) {
        this.payslips = payslips;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Employee employee = (Employee) o;
        if (employee.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), employee.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", employeeNumber='" + getEmployeeNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", terminationDate='" + getTerminationDate() + "'" +
            ", salary='" + getSalary() + "'" +
            ", pensionContribution='" + getPensionContribution() + "'" +
            ", primaryLanguage='" + getPrimaryLanguage() + "'" +
            ", secondaryLanguage='" + getSecondaryLanguage() + "'" +
            "}";
    }

    public String getDisplayName() {

        return "[" + getId() + "] " + getFirstName() + " " + getLastName() + " (Employee# " + getEmployeeNumber() + ")";
    }
}
