package com.totallynifty.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * The Payslip entity.
 */
@ApiModel(description = "The Payslip entity.")
@Entity
@Table(name = "payslip")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "payslip")
public class Payslip implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "pay_period", nullable = false)
    private LocalDate payPeriod;

    @Column(name = "gross_income", precision=10, scale=2)
    private BigDecimal grossIncome;

    @Column(name = "income_tax", precision=10, scale=2)
    private BigDecimal incomeTax;

    @Column(name = "net_income", precision=10, scale=2)
    private BigDecimal netIncome;

    @Column(name = "pension", precision=10, scale=2)
    private BigDecimal pension;

    @ManyToOne
    private Employee employee;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPayPeriod() {
        return payPeriod;
    }

    public Payslip payPeriod(LocalDate payPeriod) {
        this.payPeriod = payPeriod;
        return this;
    }

    public void setPayPeriod(LocalDate payPeriod) {
        this.payPeriod = payPeriod;
    }

    public BigDecimal getGrossIncome() {
        return grossIncome;
    }

    public Payslip grossIncome(BigDecimal grossIncome) {
        this.grossIncome = grossIncome;
        return this;
    }

    public void setGrossIncome(BigDecimal grossIncome) {
        this.grossIncome = grossIncome;
    }

    public BigDecimal getIncomeTax() {
        return incomeTax;
    }

    public Payslip incomeTax(BigDecimal incomeTax) {
        this.incomeTax = incomeTax;
        return this;
    }

    public void setIncomeTax(BigDecimal incomeTax) {
        this.incomeTax = incomeTax;
    }

    public BigDecimal getNetIncome() {
        return netIncome;
    }

    public Payslip netIncome(BigDecimal netIncome) {
        this.netIncome = netIncome;
        return this;
    }

    public void setNetIncome(BigDecimal netIncome) {
        this.netIncome = netIncome;
    }

    public BigDecimal getPension() {
        return pension;
    }

    public Payslip pension(BigDecimal pension) {
        this.pension = pension;
        return this;
    }

    public void setPension(BigDecimal pension) {
        this.pension = pension;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Payslip employee(Employee employee) {
        this.employee = employee;
        return this;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Payslip payslip = (Payslip) o;
        if (payslip.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payslip.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Payslip{" +
            "id=" + getId() +
            ", payPeriod='" + getPayPeriod() + "'" +
            ", grossIncome='" + getGrossIncome() + "'" +
            ", incomeTax='" + getIncomeTax() + "'" +
            ", netIncome='" + getNetIncome() + "'" +
            ", pension='" + getPension() + "'" +
            "}";
    }
}
