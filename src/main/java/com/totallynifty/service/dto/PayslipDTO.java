package com.totallynifty.service.dto;


import com.totallynifty.domain.Payslip;

import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * A DTO for the Payslip entity.
 */
public class PayslipDTO implements Serializable {

    private Long id;

    @NotNull
    private LocalDate payPeriod;

    private BigDecimal grossIncome;

    private BigDecimal incomeTax;

    private BigDecimal netIncome;

    private BigDecimal pension;

    private Long employeeId;

    private String displayName;

    public PayslipDTO() {
    }

    public PayslipDTO(Payslip payslip) {
        this(payslip.getId(), payslip.getPayPeriod(), payslip.getGrossIncome(), payslip.getIncomeTax(),
            payslip.getNetIncome(), payslip.getPension(), payslip.getEmployee().getId(), payslip.getEmployee().getDisplayName());
    }

    public PayslipDTO(Long id, LocalDate payPeriod, BigDecimal grossIncome, BigDecimal incomeTax, BigDecimal netIncome, BigDecimal pension,
                      Long employeeId, String displayName) {
        this.id = id;
        this.payPeriod = payPeriod;
        this.grossIncome = grossIncome;
        this.incomeTax = incomeTax;
        this.netIncome = netIncome;
        this.pension = pension;
        this.employeeId = employeeId;
        this.displayName = displayName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPayPeriod() {
        return payPeriod;
    }

    public void setPayPeriod(LocalDate payPeriod) {
        this.payPeriod = payPeriod;
    }

    public BigDecimal getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(BigDecimal grossIncome) {
        this.grossIncome = grossIncome;
    }

    public BigDecimal getIncomeTax() {
        return incomeTax;
    }

    public void setIncomeTax(BigDecimal incomeTax) {
        this.incomeTax = incomeTax;
    }

    public BigDecimal getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(BigDecimal netIncome) {
        this.netIncome = netIncome;
    }

    public BigDecimal getPension() {
        return pension;
    }

    public void setPension(BigDecimal pension) {
        this.pension = pension;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PayslipDTO payslipDTO = (PayslipDTO) o;
        if(payslipDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), payslipDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PayslipDTO{" +
            "id=" + getId() +
            ", payPeriod='" + getPayPeriod() + "'" +
            ", displayName='" + getDisplayName() + "'" +
            ", grossIncome='" + getGrossIncome() + "'" +
            ", incomeTax='" + getIncomeTax() + "'" +
            ", netIncome='" + getNetIncome() + "'" +
            ", pension='" + getPension() + "'" +
            "}";
    }
}
