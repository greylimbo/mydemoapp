package com.totallynifty.service.dto;


import java.time.LocalDate;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Objects;
import java.util.stream.Collectors;

import com.totallynifty.domain.Employee;
import com.totallynifty.domain.Payslip;
import com.totallynifty.domain.enumeration.Language;
import com.totallynifty.domain.enumeration.Language;

/**
 * A DTO for the Employee entity.
 */
public class EmployeeDTO implements Serializable {

    private Long id;

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    @Size(min = 5, max = 5)
    private String employeeNumber;

    @NotNull
    @Size(min = 5, max = 150)
    private String email;

    @Size(min = 5, max = 12)
    private String phoneNumber;

    @NotNull
    private LocalDate startDate;

    private LocalDate terminationDate;

    @NotNull
    @DecimalMin(value = "0")
    private BigDecimal salary;

    @NotNull
    @Min(value = 0)
    @Max(value = 50)
    private Integer pensionContribution;

    private Language primaryLanguage;

    private Language secondaryLanguage;

    private List<PayslipDTO> payslips;

    public EmployeeDTO() {

    }

    public EmployeeDTO(Employee employee) {
        this(employee.getId(), employee.getFirstName(), employee.getLastName(), employee.getEmployeeNumber(), employee.getEmail(), employee.getPhoneNumber(),
            employee.getStartDate(), employee.getTerminationDate(), employee.getSalary(), employee.getPensionContribution(), employee.getPrimaryLanguage(),
            employee.getSecondaryLanguage());
    }

    public EmployeeDTO(Long id, String firstName, String lastName, String employeeNumber, String email, String phoneNumber, LocalDate startDate, LocalDate terminationDate,
                       BigDecimal salary, Integer pensionContribution, Language primaryLanguage, Language secondaryLanguage) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeNumber = employeeNumber;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.startDate = startDate;
        this.terminationDate = terminationDate;
        this.salary = salary;
        this.pensionContribution = pensionContribution;
        this.primaryLanguage = primaryLanguage;
        this.secondaryLanguage = secondaryLanguage;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getTerminationDate() {
        return terminationDate;
    }

    public void setTerminationDate(LocalDate terminationDate) {
        this.terminationDate = terminationDate;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public Integer getPensionContribution() {
        return pensionContribution;
    }

    public void setPensionContribution(Integer pensionContribution) {
        this.pensionContribution = pensionContribution;
    }

    public Language getPrimaryLanguage() {
        return primaryLanguage;
    }

    public void setPrimaryLanguage(Language primaryLanguage) {
        this.primaryLanguage = primaryLanguage;
    }

    public Language getSecondaryLanguage() {
        return secondaryLanguage;
    }

    public void setSecondaryLanguage(Language secondaryLanguage) {
        this.secondaryLanguage = secondaryLanguage;
    }

    public List<PayslipDTO> getPayslips() {
        return payslips;
    }

    public void setPayslips(List<PayslipDTO> payslips) {
        this.payslips = payslips;
    }

    public int getNumPayslips() {
        int numPaySlips = 0;
        if (this.payslips != null)
            numPaySlips = this.payslips.size();
        return numPaySlips;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EmployeeDTO employeeDTO = (EmployeeDTO) o;
        if(employeeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), employeeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EmployeeDTO{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", employeeNumber='" + getEmployeeNumber() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", terminationDate='" + getTerminationDate() + "'" +
            ", salary='" + getSalary() + "'" +
            ", pensionContribution='" + getPensionContribution() + "'" +
            ", primaryLanguage='" + getPrimaryLanguage() + "'" +
            ", secondaryLanguage='" + getSecondaryLanguage() + "'" +
            "}";
    }
}
