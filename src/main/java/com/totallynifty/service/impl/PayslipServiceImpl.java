package com.totallynifty.service.impl;

import com.totallynifty.service.PayslipService;
import com.totallynifty.domain.Payslip;
import com.totallynifty.repository.PayslipRepository;
import com.totallynifty.repository.search.PayslipSearchRepository;
import com.totallynifty.service.dto.PayslipDTO;
import com.totallynifty.service.mapper.PayslipMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Payslip.
 */
@Service
@Transactional
public class PayslipServiceImpl implements PayslipService{

    private final Logger log = LoggerFactory.getLogger(PayslipServiceImpl.class);

    private final PayslipRepository payslipRepository;

    private final PayslipMapper payslipMapper;

    private final PayslipSearchRepository payslipSearchRepository;

    public PayslipServiceImpl(PayslipRepository payslipRepository, PayslipMapper payslipMapper, PayslipSearchRepository payslipSearchRepository) {
        this.payslipRepository = payslipRepository;
        this.payslipMapper = payslipMapper;
        this.payslipSearchRepository = payslipSearchRepository;
    }

    /**
     * Save a payslip.
     *
     * @param payslipDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PayslipDTO save(PayslipDTO payslipDTO) {
        log.debug("Request to save Payslip : {}", payslipDTO);
        Payslip payslip = payslipMapper.toEntity(payslipDTO);
        payslip = payslipRepository.save(payslip);
        PayslipDTO result = payslipMapper.entityToDTO(payslip);
        payslipSearchRepository.save(payslip);
        return result;
    }

    /**
     *  Get all the payslips.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PayslipDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Payslips");
        return payslipRepository.findAll(pageable)
            .map(payslipMapper::entityToDTO);
    }

    /**
     *  Get one payslip by id.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public PayslipDTO findOne(Long id) {
        log.debug("Request to get Payslip : {}", id);
        Payslip payslip = payslipRepository.findOne(id);
        PayslipDTO payslipDTO = payslipMapper.entityToDTO(payslip);
//        payslipDTO.setDisplayName(payslip.getEmployee().getDisplayName());
        return payslipDTO;
    }

    /**
     *  Delete the  payslip by id.
     *
     *  @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Payslip : {}", id);
        payslipRepository.delete(id);
        payslipSearchRepository.delete(id);
    }

    /**
     * Search for the payslip corresponding to the query.
     *
     *  @param query the query of the search
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<PayslipDTO> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Payslips for query {}", query);
        Page<Payslip> result = payslipSearchRepository.search(queryStringQuery(query), pageable);
        Page<PayslipDTO> payslipDTOS = result.map(payslipMapper::entityToDTO);
        return payslipDTOS;
    }
}
