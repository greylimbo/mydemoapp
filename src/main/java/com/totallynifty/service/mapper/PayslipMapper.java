package com.totallynifty.service.mapper;

import com.totallynifty.domain.*;
import com.totallynifty.service.dto.PayslipDTO;

import org.mapstruct.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Payslip and its DTO PayslipDTO.
 */
@Mapper(componentModel = "spring", uses = {EmployeeMapper.class, })
public interface PayslipMapper extends EntityMapper <PayslipDTO, Payslip> {

    @Override
    default List<Payslip> toEntityList(List<PayslipDTO> payslipDTOS) {
        return payslipDTOS.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    default List<PayslipDTO> toDto(List<Payslip> payslips) {
        return payslips.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }

    @Mappings({
        @Mapping(source = "employee.id", target = "employeeId"),
        @Mapping(source = "employee.displayName", target = "displayName")
    })
    PayslipDTO toDto(Payslip payslip);

    default PayslipDTO entityToDTO(Payslip entity) {
        PayslipDTO dto = new PayslipDTO(entity);
        dto.setDisplayName(entity.getEmployee().getDisplayName());
        return dto;
    }

    @Mapping(source = "employeeId", target = "employee")
    Payslip toEntity(PayslipDTO payslipDTO);
    default Payslip fromId(Long id) {
        if (id == null) {
            return null;
        }
        Payslip payslip = new Payslip();
        payslip.setId(id);
        return payslip;
    }
}
