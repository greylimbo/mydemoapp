package com.totallynifty.service.mapper;

import com.totallynifty.domain.*;
import com.totallynifty.service.dto.EmployeeDTO;

import org.mapstruct.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Mapper for the entity Employee and its DTO EmployeeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface EmployeeMapper extends EntityMapper <EmployeeDTO, Employee> {

    @Override
    default List<Employee> toEntityList(List<EmployeeDTO> employeeDTOS) {
        return employeeDTOS.stream()
            .filter(Objects::nonNull)
            .map(this::toEntity)
            .collect(Collectors.toList());
    }

    @Override
    default List<EmployeeDTO> toDto(List<Employee> employees) {
        return employees.stream()
            .filter(Objects::nonNull)
            .map(this::toDto)
            .collect(Collectors.toList());
    }

    @Override
    default EmployeeDTO toDto(Employee entity) {
        return new EmployeeDTO(entity);
    }

    @Mapping(target = "payslips", ignore = true)
    Employee toEntity(EmployeeDTO employeeDTO);
    default Employee fromId(Long id) {
        if (id == null) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        return employee;
    }
}
