package com.totallynifty.service.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by rail on 2017/07/03.
 */
public final class TaxUtil {

    private static final BigDecimal TAX_BRACKET_01 = new BigDecimal(18200);
    private static final BigDecimal TAX_BRACKET_02 = new BigDecimal(37000);
    private static final BigDecimal TAX_BRACKET_03 = new BigDecimal(80000);
    private static final BigDecimal TAX_BRACKET_04 = new BigDecimal(180000);

    private static final BigDecimal TAX_PERC_01 = new BigDecimal(0.19);
    private static final BigDecimal TAX_PERC_02 = new BigDecimal(0.325);
    private static final BigDecimal TAX_PERC_03 = new BigDecimal(0.37);
    private static final BigDecimal TAX_PERC_04 = new BigDecimal(0.45);

    private static final BigDecimal TAX_INCR_02 = new BigDecimal(3572);
    private static final BigDecimal TAX_INCR_03 = new BigDecimal(17547);
    private static final BigDecimal TAX_INCR_04 = new BigDecimal(54547);

    public static final BigDecimal MONTHS = new BigDecimal(12);

    public static BigDecimal calculateTax(BigDecimal income) {

        BigDecimal taxableAmount = BigDecimal.ZERO;

        if (income.compareTo(TAX_BRACKET_01) > 0
                && income.compareTo(TAX_BRACKET_02) <= 0)
            taxableAmount = (income.subtract(TAX_BRACKET_01).multiply(TAX_PERC_01));

        else if (income.compareTo(TAX_BRACKET_02) > 0
            && income.compareTo(TAX_BRACKET_03) <= 0)
            taxableAmount = TAX_INCR_02.add(income.subtract(TAX_BRACKET_02).multiply(TAX_PERC_02));

        else if (income.compareTo(TAX_BRACKET_03) > 0
            && income.compareTo(TAX_BRACKET_04) <= 0)
            taxableAmount = TAX_INCR_03.add(income.subtract(TAX_BRACKET_03).multiply(TAX_PERC_03));

        else if (income.compareTo(TAX_BRACKET_04) > 0)
            taxableAmount = TAX_INCR_04.add(income.subtract(TAX_BRACKET_04).multiply(TAX_PERC_04));

        return (taxableAmount.divide(MONTHS, 0, RoundingMode.CEILING)).setScale(0, RoundingMode.CEILING);
    }

    public static BigDecimal calculateGrossIncome(BigDecimal income) {

        if (income.compareTo(BigDecimal.ZERO) > 0)
            return (income.divide(MONTHS, 0, RoundingMode.FLOOR)).setScale(0, RoundingMode.FLOOR);

        return BigDecimal.ZERO;
    }

    public static BigDecimal calculatePensionContribution(BigDecimal grossIncome, Integer percentage) {

        BigDecimal contribution = new BigDecimal(percentage/100.0);
        return (grossIncome.multiply(contribution)).setScale(0, RoundingMode.FLOOR);

    }
}
