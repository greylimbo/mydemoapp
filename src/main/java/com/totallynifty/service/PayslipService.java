package com.totallynifty.service;

import com.totallynifty.service.dto.PayslipDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing Payslip.
 */
public interface PayslipService {

    /**
     * Save a payslip.
     *
     * @param payslipDTO the entity to save
     * @return the persisted entity
     */
    PayslipDTO save(PayslipDTO payslipDTO);

    /**
     *  Get all the payslips.
     *
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PayslipDTO> findAll(Pageable pageable);

    /**
     *  Get the "id" payslip.
     *
     *  @param id the id of the entity
     *  @return the entity
     */
    PayslipDTO findOne(Long id);

    /**
     *  Delete the "id" payslip.
     *
     *  @param id the id of the entity
     */
    void delete(Long id);

    /**
     * Search for the payslip corresponding to the query.
     *
     *  @param query the query of the search
     *  
     *  @param pageable the pagination information
     *  @return the list of entities
     */
    Page<PayslipDTO> search(String query, Pageable pageable);
}
