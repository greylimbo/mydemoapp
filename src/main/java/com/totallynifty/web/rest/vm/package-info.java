/**
 * View Models used by Spring MVC REST controllers.
 */
package com.totallynifty.web.rest.vm;
