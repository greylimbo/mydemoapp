package com.totallynifty.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.totallynifty.service.EmployeeService;
import com.totallynifty.service.PayslipService;
import com.totallynifty.service.dto.EmployeeDTO;
import com.totallynifty.service.util.TaxUtil;
import com.totallynifty.web.rest.util.HeaderUtil;
import com.totallynifty.web.rest.util.PaginationUtil;
import com.totallynifty.service.dto.PayslipDTO;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Payslip.
 */
@RestController
@RequestMapping("/api")
public class PayslipResource {

    private final Logger log = LoggerFactory.getLogger(PayslipResource.class);

    private static final String ENTITY_NAME = "payslip";

    private final PayslipService payslipService;
    private final EmployeeService employeeService;

    public PayslipResource(PayslipService payslipService, EmployeeService employeeService) {
        this.payslipService = payslipService;
        this.employeeService = employeeService;
    }

    /**
     * POST  /payslips : Create a new payslip.
     *
     * @param payslipDTO the payslipDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new payslipDTO, or with status 400 (Bad Request) if the payslip has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/payslips")
    @Timed
    public ResponseEntity<PayslipDTO> createPayslip(@Valid @RequestBody PayslipDTO payslipDTO) throws URISyntaxException {
        log.debug("REST request to save Payslip : {}", payslipDTO);
        if (payslipDTO.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new payslip cannot already have an ID")).body(null);
        }

        /**
         * Perform appropriate calculations, preferably the values for the tax calculation
         * should be stored and then referenced from the database.
         *
         * Also assumes a check is made  to ensure no other Payslip exists for the same Employee and Pay Period.
         */
        EmployeeDTO employeeDTO = employeeService.findOne(payslipDTO.getEmployeeId());
        BigDecimal grossIncome = TaxUtil.calculateGrossIncome(employeeDTO.getSalary());
        BigDecimal tax = TaxUtil.calculateTax(employeeDTO.getSalary());

        payslipDTO.setGrossIncome(grossIncome);
        payslipDTO.setIncomeTax(tax);
        payslipDTO.setNetIncome(grossIncome.subtract(tax));
        payslipDTO.setPension(TaxUtil.calculatePensionContribution(grossIncome, employeeDTO.getPensionContribution()));

        PayslipDTO result = payslipService.save(payslipDTO);
        return ResponseEntity.created(new URI("/api/payslips/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /payslips : Updates an existing payslip.
     *
     * @param payslipDTO the payslipDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated payslipDTO,
     * or with status 400 (Bad Request) if the payslipDTO is not valid,
     * or with status 500 (Internal Server Error) if the payslipDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/payslips")
    @Timed
    public ResponseEntity<PayslipDTO> updatePayslip(@Valid @RequestBody PayslipDTO payslipDTO) throws URISyntaxException {
        log.debug("REST request to update Payslip : {}", payslipDTO);
        if (payslipDTO.getId() == null) {
            return createPayslip(payslipDTO);
        }
        PayslipDTO result = payslipService.save(payslipDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, payslipDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /payslips : get all the payslips.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of payslips in body
     */
    @GetMapping("/payslips")
    @Timed
    public ResponseEntity<List<PayslipDTO>> getAllPayslips(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Payslips");
        Page<PayslipDTO> page = payslipService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payslips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /payslips/:id : get the "id" payslip.
     *
     * @param id the id of the payslipDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the payslipDTO, or with status 404 (Not Found)
     */
    @GetMapping("/payslips/{id}")
    @Timed
    public ResponseEntity<PayslipDTO> getPayslip(@PathVariable Long id) {
        log.debug("REST request to get Payslip : {}", id);
        PayslipDTO payslipDTO = payslipService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(payslipDTO));
    }

    /**
     * DELETE  /payslips/:id : delete the "id" payslip.
     *
     * @param id the id of the payslipDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/payslips/{id}")
    @Timed
    public ResponseEntity<Void> deletePayslip(@PathVariable Long id) {
        log.debug("REST request to delete Payslip : {}", id);
        payslipService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/payslips?query=:query : search for the payslip corresponding
     * to the query.
     *
     * @param query the query of the payslip search
     * @param pageable the pagination information
     * @return the result of the search
     */
    @GetMapping("/_search/payslips")
    @Timed
    public ResponseEntity<List<PayslipDTO>> searchPayslips(@RequestParam String query, @ApiParam Pageable pageable) {
        log.debug("REST request to search for a page of Payslips for query {}", query);
        Page<PayslipDTO> page = payslipService.search(query, pageable);
        HttpHeaders headers = PaginationUtil.generateSearchPaginationHttpHeaders(query, page, "/api/_search/payslips");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

}
