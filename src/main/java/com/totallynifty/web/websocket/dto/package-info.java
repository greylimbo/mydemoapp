/**
 * Data Access Objects used by WebSocket services.
 */
package com.totallynifty.web.websocket.dto;
