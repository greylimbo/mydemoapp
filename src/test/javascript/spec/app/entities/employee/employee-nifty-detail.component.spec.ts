import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NiftyappTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { EmployeeNiftyDetailComponent } from '../../../../../../main/webapp/app/entities/employee/employee-nifty-detail.component';
import { EmployeeNiftyService } from '../../../../../../main/webapp/app/entities/employee/employee-nifty.service';
import { EmployeeNifty } from '../../../../../../main/webapp/app/entities/employee/employee-nifty.model';

describe('Component Tests', () => {

    describe('EmployeeNifty Management Detail Component', () => {
        let comp: EmployeeNiftyDetailComponent;
        let fixture: ComponentFixture<EmployeeNiftyDetailComponent>;
        let service: EmployeeNiftyService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [NiftyappTestModule],
                declarations: [EmployeeNiftyDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    EmployeeNiftyService,
                    JhiEventManager
                ]
            }).overrideTemplate(EmployeeNiftyDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EmployeeNiftyDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EmployeeNiftyService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new EmployeeNifty(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.employee).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
