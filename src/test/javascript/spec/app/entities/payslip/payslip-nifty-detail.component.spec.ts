import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { JhiDateUtils, JhiDataUtils, JhiEventManager } from 'ng-jhipster';
import { NiftyappTestModule } from '../../../test.module';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { PayslipNiftyDetailComponent } from '../../../../../../main/webapp/app/entities/payslip/payslip-nifty-detail.component';
import { PayslipNiftyService } from '../../../../../../main/webapp/app/entities/payslip/payslip-nifty.service';
import { PayslipNifty } from '../../../../../../main/webapp/app/entities/payslip/payslip-nifty.model';

describe('Component Tests', () => {

    describe('PayslipNifty Management Detail Component', () => {
        let comp: PayslipNiftyDetailComponent;
        let fixture: ComponentFixture<PayslipNiftyDetailComponent>;
        let service: PayslipNiftyService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [NiftyappTestModule],
                declarations: [PayslipNiftyDetailComponent],
                providers: [
                    JhiDateUtils,
                    JhiDataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    PayslipNiftyService,
                    JhiEventManager
                ]
            }).overrideTemplate(PayslipNiftyDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PayslipNiftyDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PayslipNiftyService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new PayslipNifty(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.payslip).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
