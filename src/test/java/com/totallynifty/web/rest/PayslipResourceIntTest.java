package com.totallynifty.web.rest;

import com.totallynifty.NiftyappApp;

import com.totallynifty.domain.Payslip;
import com.totallynifty.repository.PayslipRepository;
import com.totallynifty.service.EmployeeService;
import com.totallynifty.service.PayslipService;
import com.totallynifty.repository.search.PayslipSearchRepository;
import com.totallynifty.service.dto.PayslipDTO;
import com.totallynifty.service.mapper.PayslipMapper;
import com.totallynifty.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PayslipResource REST controller.
 *
 * @see PayslipResource
 */
@Ignore
@RunWith(SpringRunner.class)
@SpringBootTest(classes = NiftyappApp.class)
public class PayslipResourceIntTest {

    private static final LocalDate DEFAULT_PAY_PERIOD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_PAY_PERIOD = LocalDate.now(ZoneId.systemDefault());

    private static final BigDecimal DEFAULT_GROSS_INCOME = new BigDecimal(1);
    private static final BigDecimal UPDATED_GROSS_INCOME = new BigDecimal(2);

    private static final BigDecimal DEFAULT_INCOME_TAX = new BigDecimal(1);
    private static final BigDecimal UPDATED_INCOME_TAX = new BigDecimal(2);

    private static final BigDecimal DEFAULT_NET_INCOME = new BigDecimal(1);
    private static final BigDecimal UPDATED_NET_INCOME = new BigDecimal(2);

    private static final BigDecimal DEFAULT_PENSION = new BigDecimal(1);
    private static final BigDecimal UPDATED_PENSION = new BigDecimal(2);

    /*@Autowired
    private PayslipRepository payslipRepository;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private PayslipMapper payslipMapper;

    @Autowired
    private PayslipService payslipService;

    @Autowired
    private PayslipSearchRepository payslipSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPayslipMockMvc;

    private Payslip payslip;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        PayslipResource payslipResource = new PayslipResource(payslipService, employeeService);
        this.restPayslipMockMvc = MockMvcBuilders.standaloneSetup(payslipResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    *//**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     *//*
    public static Payslip createEntity(EntityManager em) {
        Payslip payslip = new Payslip()
            .payPeriod(DEFAULT_PAY_PERIOD)
            .grossIncome(DEFAULT_GROSS_INCOME)
            .incomeTax(DEFAULT_INCOME_TAX)
            .netIncome(DEFAULT_NET_INCOME)
            .pension(DEFAULT_PENSION);
        return payslip;
    }

    @Before
    public void initTest() {
        payslipSearchRepository.deleteAll();
        payslip = createEntity(em);
    }

    @Test
    @Transactional
    public void createPayslip() throws Exception {
        int databaseSizeBeforeCreate = payslipRepository.findAll().size();

        // Create the Payslip
        PayslipDTO payslipDTO = payslipMapper.toDto(payslip);
        restPayslipMockMvc.perform(post("/api/payslips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payslipDTO)))
            .andExpect(status().isCreated());

        // Validate the Payslip in the database
        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeCreate + 1);
        Payslip testPayslip = payslipList.get(payslipList.size() - 1);
        assertThat(testPayslip.getPayPeriod()).isEqualTo(DEFAULT_PAY_PERIOD);
        assertThat(testPayslip.getGrossIncome()).isEqualTo(DEFAULT_GROSS_INCOME);
        assertThat(testPayslip.getIncomeTax()).isEqualTo(DEFAULT_INCOME_TAX);
        assertThat(testPayslip.getNetIncome()).isEqualTo(DEFAULT_NET_INCOME);
        assertThat(testPayslip.getPension()).isEqualTo(DEFAULT_PENSION);

        // Validate the Payslip in Elasticsearch
        Payslip payslipEs = payslipSearchRepository.findOne(testPayslip.getId());
        assertThat(payslipEs).isEqualToComparingFieldByField(testPayslip);
    }

    @Test
    @Transactional
    public void createPayslipWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = payslipRepository.findAll().size();

        // Create the Payslip with an existing ID
        payslip.setId(1L);
        PayslipDTO payslipDTO = payslipMapper.toDto(payslip);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPayslipMockMvc.perform(post("/api/payslips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payslipDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkPayPeriodIsRequired() throws Exception {
        int databaseSizeBeforeTest = payslipRepository.findAll().size();
        // set the field null
        payslip.setPayPeriod(null);

        // Create the Payslip, which fails.
        PayslipDTO payslipDTO = payslipMapper.toDto(payslip);

        restPayslipMockMvc.perform(post("/api/payslips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payslipDTO)))
            .andExpect(status().isBadRequest());

        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllPayslips() throws Exception {
        // Initialize the database
        payslipRepository.saveAndFlush(payslip);

        // Get all the payslipList
        restPayslipMockMvc.perform(get("/api/payslips?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payslip.getId().intValue())))
            .andExpect(jsonPath("$.[*].payPeriod").value(hasItem(DEFAULT_PAY_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].grossIncome").value(hasItem(DEFAULT_GROSS_INCOME.intValue())))
            .andExpect(jsonPath("$.[*].incomeTax").value(hasItem(DEFAULT_INCOME_TAX.intValue())))
            .andExpect(jsonPath("$.[*].netIncome").value(hasItem(DEFAULT_NET_INCOME.intValue())))
            .andExpect(jsonPath("$.[*].pension").value(hasItem(DEFAULT_PENSION.intValue())));
    }

    @Test
    @Transactional
    public void getPayslip() throws Exception {
        // Initialize the database
        payslipRepository.saveAndFlush(payslip);

        // Get the payslip
        restPayslipMockMvc.perform(get("/api/payslips/{id}", payslip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(payslip.getId().intValue()))
            .andExpect(jsonPath("$.payPeriod").value(DEFAULT_PAY_PERIOD.toString()))
            .andExpect(jsonPath("$.grossIncome").value(DEFAULT_GROSS_INCOME.intValue()))
            .andExpect(jsonPath("$.incomeTax").value(DEFAULT_INCOME_TAX.intValue()))
            .andExpect(jsonPath("$.netIncome").value(DEFAULT_NET_INCOME.intValue()))
            .andExpect(jsonPath("$.pension").value(DEFAULT_PENSION.intValue()));
    }

    @Test
    @Transactional
    public void getNonExistingPayslip() throws Exception {
        // Get the payslip
        restPayslipMockMvc.perform(get("/api/payslips/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePayslip() throws Exception {
        // Initialize the database
        payslipRepository.saveAndFlush(payslip);
        payslipSearchRepository.save(payslip);
        int databaseSizeBeforeUpdate = payslipRepository.findAll().size();

        // Update the payslip
        Payslip updatedPayslip = payslipRepository.findOne(payslip.getId());
        updatedPayslip
            .payPeriod(UPDATED_PAY_PERIOD)
            .grossIncome(UPDATED_GROSS_INCOME)
            .incomeTax(UPDATED_INCOME_TAX)
            .netIncome(UPDATED_NET_INCOME)
            .pension(UPDATED_PENSION);
        PayslipDTO payslipDTO = payslipMapper.toDto(updatedPayslip);

        restPayslipMockMvc.perform(put("/api/payslips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payslipDTO)))
            .andExpect(status().isOk());

        // Validate the Payslip in the database
        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeUpdate);
        Payslip testPayslip = payslipList.get(payslipList.size() - 1);
        assertThat(testPayslip.getPayPeriod()).isEqualTo(UPDATED_PAY_PERIOD);
        assertThat(testPayslip.getGrossIncome()).isEqualTo(UPDATED_GROSS_INCOME);
        assertThat(testPayslip.getIncomeTax()).isEqualTo(UPDATED_INCOME_TAX);
        assertThat(testPayslip.getNetIncome()).isEqualTo(UPDATED_NET_INCOME);
        assertThat(testPayslip.getPension()).isEqualTo(UPDATED_PENSION);

        // Validate the Payslip in Elasticsearch
        Payslip payslipEs = payslipSearchRepository.findOne(testPayslip.getId());
        assertThat(payslipEs).isEqualToComparingFieldByField(testPayslip);
    }

    @Test
    @Transactional
    public void updateNonExistingPayslip() throws Exception {
        int databaseSizeBeforeUpdate = payslipRepository.findAll().size();

        // Create the Payslip
        PayslipDTO payslipDTO = payslipMapper.toDto(payslip);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPayslipMockMvc.perform(put("/api/payslips")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(payslipDTO)))
            .andExpect(status().isCreated());

        // Validate the Payslip in the database
        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deletePayslip() throws Exception {
        // Initialize the database
        payslipRepository.saveAndFlush(payslip);
        payslipSearchRepository.save(payslip);
        int databaseSizeBeforeDelete = payslipRepository.findAll().size();

        // Get the payslip
        restPayslipMockMvc.perform(delete("/api/payslips/{id}", payslip.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean payslipExistsInEs = payslipSearchRepository.exists(payslip.getId());
        assertThat(payslipExistsInEs).isFalse();

        // Validate the database is empty
        List<Payslip> payslipList = payslipRepository.findAll();
        assertThat(payslipList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchPayslip() throws Exception {
        // Initialize the database
        payslipRepository.saveAndFlush(payslip);
        payslipSearchRepository.save(payslip);

        // Search the payslip
        restPayslipMockMvc.perform(get("/api/_search/payslips?query=id:" + payslip.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(payslip.getId().intValue())))
            .andExpect(jsonPath("$.[*].payPeriod").value(hasItem(DEFAULT_PAY_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].grossIncome").value(hasItem(DEFAULT_GROSS_INCOME.intValue())))
            .andExpect(jsonPath("$.[*].incomeTax").value(hasItem(DEFAULT_INCOME_TAX.intValue())))
            .andExpect(jsonPath("$.[*].netIncome").value(hasItem(DEFAULT_NET_INCOME.intValue())))
            .andExpect(jsonPath("$.[*].pension").value(hasItem(DEFAULT_PENSION.intValue())));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Payslip.class);
        Payslip payslip1 = new Payslip();
        payslip1.setId(1L);
        Payslip payslip2 = new Payslip();
        payslip2.setId(payslip1.getId());
        assertThat(payslip1).isEqualTo(payslip2);
        payslip2.setId(2L);
        assertThat(payslip1).isNotEqualTo(payslip2);
        payslip1.setId(null);
        assertThat(payslip1).isNotEqualTo(payslip2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PayslipDTO.class);
        PayslipDTO payslipDTO1 = new PayslipDTO();
        payslipDTO1.setId(1L);
        PayslipDTO payslipDTO2 = new PayslipDTO();
        assertThat(payslipDTO1).isNotEqualTo(payslipDTO2);
        payslipDTO2.setId(payslipDTO1.getId());
        assertThat(payslipDTO1).isEqualTo(payslipDTO2);
        payslipDTO2.setId(2L);
        assertThat(payslipDTO1).isNotEqualTo(payslipDTO2);
        payslipDTO1.setId(null);
        assertThat(payslipDTO1).isNotEqualTo(payslipDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(payslipMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(payslipMapper.fromId(null)).isNull();
    }*/
}
