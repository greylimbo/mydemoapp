DEFAULT_MIN = 0
DEFAULT_MAX = 50

/**
 * The Employee entity.
 */
entity Employee {
	/**
	* The firstname attribute.
	*/
	firstName String required,
	lastName String required,
    employeeNumber String required minlength(5) maxlength(5),
	email String required minlength(5) maxlength(150),
	phoneNumber String minlength(5) maxlength(12),
	startDate LocalDate required,
    terminationDate LocalDate,
	salary BigDecimal required min(DEFAULT_MIN),
	pensionContribution Integer required min(DEFAULT_MIN) max(DEFAULT_MAX),
    primaryLanguage Language,
    secondaryLanguage Language
}

/**
 * The Payslip entity.
 */
entity Payslip {
	payPeriod LocalDate required,
	grossIncome BigDecimal,
    incomeTax BigDecimal,
    netIncome BigDecimal,
    pension BigDecimal
}

enum Language {
    ENGLISH, AFRIKAANS, ZULU, XHOSA, SWATI, OTHER
}

// defining multiple OneToMany relationships with comments
relationship OneToMany {
	Employee{payslip} to Payslip
}

// Set pagination options
paginate Employee, Payslip with infinite-scroll

dto * with mapstruct

// Set service options to all except few
service all with serviceImpl
// Set an angular suffix
angularSuffix * with nifty
