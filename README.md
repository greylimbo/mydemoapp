# niftyapp
This application was generated using JHipster 4.5.6, it utilizes a MySQL Database appropriately named "niftyapp" with an appropriate user with appropriate rights called "niftyapp", pwrd: "niftyapp"..

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.
2. [Yarn][]: We use Yarn to manage Node dependencies.
   Depending on your system, you can install Yarn either from source or as a pre-packaged bundle.

We use yarn scripts and [Webpack][] as our build system.


Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./gradlew
    yarn start

Then navigate to [http://localhost:9000](http://localhost:9000) in your browser.